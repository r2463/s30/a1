const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const PORT = 3008;



app.use(express.json())
app.use(express.urlencoded({ extended: true }))


mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))


// SCHEMA
const signUpSchema = new mongoose.Schema(

    {
        username: {

            type: String,
            required: [true, `Name is required`]
        },
        password: {
            type: String,
            required: [true, `Password is required`]
        }
    }
)

// MODEL
const SignUp = mongoose.model(`SignUp`, signUpSchema)


// ROUTE

app.post('/signup', async(req, res) => {

    await SignUp.findOne({username: req.body.username}).then( (result, err) => {
		console.log(result)


        if (result != null && result.username === req.body.username) {
            return res.send(`Duplicate user found`)


        } else {

            let newUser = new SignUp({

                username: req.body.username,
                password: req.body.password
            })

            newUser.save().then((savedUser, err) => {

                if (savedUser) {
                    return res.send(`New user registered`)
                } else {
                    return res.status(500).send(err)
                }
            })
        }

    })

   

 })



    app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))
